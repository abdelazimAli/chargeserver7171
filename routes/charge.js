var express = require('express');
var router = express.Router();
var { utilities } = require('../utilities');

/* GET home page. */
router.post('/', function(req, res, next) {
    var msisdn = req.body.msisdn;
    let valid = utilities.validateMsisdn(msisdn);
    
    if (!valid) {
        res.status(400).send({error:"Invalid Number"}).end()
    }else{
        let x = Math.round(Math.random() * 10);
        if (x>5) {
            console.log(`Number:${msisdn} is charged`);
            res.status(200).send({
                msisdn,
                status:1
            }).end();           
        }
        else{
            console.log(`Number:${msisdn} is Not charged`);
            res.status(200).send({
                msisdn,
                status:0
            }).end();     
        }

    }
  });
  
  module.exports = router;