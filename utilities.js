function validateMsisdn(msisdn) {
    var phoneno = /^\+([0-9]{1})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})[-. ]?([0-9]{3})$/;
    let res = msisdn.match(phoneno)
    if (res) {
        return true;
    } else {
        return false;
    }
}

module.exports={
    utilities:{
        validateMsisdn
    }
}